package main

import (
	"brent-oil-price/src/daily"
	"brent-oil-price/src/monthly"
	"brent-oil-price/src/today"
	"github.com/alexflint/go-arg"
	"github.com/joho/godotenv"
	"os"
)

type TodayCmd struct{}
type DailyCmd struct{}
type MonthlyCmd struct{}

var args struct {
	Today   *TodayCmd   `arg:"subcommand:today"`
	Daily   *DailyCmd   `arg:"subcommand:daily"`
	Monthly *MonthlyCmd `arg:"subcommand:monthly"`
}

func main() {
	if _, err := os.Stat(".env"); err == nil {
		err = godotenv.Load(".env")
		if err != nil {
			panic(err)
		}
	}

	p := arg.MustParse(&args)
	if p.Subcommand() == nil {
		p.Fail("missing subcommand")
	}

	switch {
	case args.Today != nil:
		today.Run()
	case args.Daily != nil:
		daily.Run()
	case args.Monthly != nil:
		monthly.Run()
	}
}
