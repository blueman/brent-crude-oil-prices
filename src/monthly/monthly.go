package monthly

import (
	"brent-oil-price/src"
	"encoding/json"
	"io"
	"os"
	"time"
)

func Run() {
	monthlyJsonData, err := monthlyFile()
	src.CheckError(err)
	lastMonth := monthlyJsonData[len(monthlyJsonData)-1]

	dailyJsonData, err := dailyFile()
	src.CheckError(err)
	dailyLength := len(dailyJsonData)

	now := time.Now()
	var pricesThisMonth []float64
	for i := 1; i <= 31; i++ {
		record := dailyJsonData[dailyLength-i]

		if record.Date.Format("01") != now.Format("01") {
			break
		}

		pricesThisMonth = append(pricesThisMonth, record.Price)
	}
	avgRoundPrice := src.RoundToNearestTwoDecimals(avg(pricesThisMonth))

	if lastMonth.Date.Format("01") != now.Format("01") {
		t1 := time.Date(now.Year(), now.Month(), 15, 0, 0, 0, 0, now.Location())
		monthlyJsonData = append(monthlyJsonData, &src.BrentPrice{
			Date:  src.CustomDateFormat(t1),
			Price: avgRoundPrice,
		})
	} else {
		lastMonth.Price = avgRoundPrice
	}

	fileData, _ := json.MarshalIndent(monthlyJsonData, "", " ")
	err = os.WriteFile(src.MonthlyFile, fileData, 0644)
	src.CheckError(err)
}

func avg(array []float64) float64 {
	sum := 0.0
	for _, v := range array {
		sum += v
	}

	return sum / float64(len(array))
}

func monthlyFile() ([]*src.BrentPrice, error) {
	file, err := os.Open(src.MonthlyFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	byteValue, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var jsonData []*src.BrentPrice
	err = json.Unmarshal(byteValue, &jsonData)
	if err != nil {
		return nil, err
	}

	return jsonData, nil
}

func dailyFile() ([]*src.BrentPrice, error) {
	file, err := os.Open(src.DailyFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	byteValue, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var jsonData []*src.BrentPrice
	err = json.Unmarshal(byteValue, &jsonData)
	if err != nil {
		return nil, err
	}

	return jsonData, nil
}
