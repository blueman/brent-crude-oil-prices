package daily

import (
	"brent-oil-price/src"
	"encoding/json"
	"io"
	"os"
)

func Run() {
	todayJsonData, err := todayFile()
	src.CheckError(err)

	dailyJsonData, err := dailyFile()
	src.CheckError(err)

	rows := len(dailyJsonData)

	lastEntry := dailyJsonData[rows-1]

	if lastEntry.Date == todayJsonData.Date {
		lastEntry.Price = todayJsonData.Price
	} else {
		dailyJsonData = append(dailyJsonData, todayJsonData)
	}

	fileData, _ := json.MarshalIndent(dailyJsonData, "", " ")
	err = os.WriteFile(src.DailyFile, fileData, 0644)
	src.CheckError(err)
}

func todayFile() (*src.BrentPrice, error) {
	file, err := os.Open(src.TodayFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	byteValue, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var jsonData *src.BrentPrice
	err = json.Unmarshal(byteValue, &jsonData)
	if err != nil {
		return nil, err
	}

	return jsonData, nil
}

func dailyFile() ([]*src.BrentPrice, error) {
	file, err := os.Open(src.DailyFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	byteValue, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var jsonData []*src.BrentPrice
	err = json.Unmarshal(byteValue, &jsonData)
	if err != nil {
		return nil, err
	}

	return jsonData, nil
}
