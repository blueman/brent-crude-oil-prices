package today

import (
	"brent-oil-price/src"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func Run() {
	stockValue, err := getContent()
	src.CheckError(err)
	fmt.Printf("%+v\n", stockValue)

	if stockValue.Price == 0 {
		fmt.Println("Price is zero, skipping")
		return
	}

	nowBrentPrice := src.BrentPrice{
		Date:  src.CustomDateFormat(time.Now()),
		Price: src.RoundToNearestTwoDecimals(stockValue.Price),
	}

	file, _ := json.MarshalIndent(nowBrentPrice, "", " ")
	err = os.WriteFile(src.TodayFile, file, 0644)
	src.CheckError(err)
}

func getContent() (*src.BrentOilStock, error) {
	url := "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes?region=US&symbols=CL%3DF"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPIDAPI_KEY"))
	req.Header.Add("X-RapidAPI-Host", os.Getenv("RAPIDAPI_HOST"))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	content, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var yahoo YahooFinanceQuote
	err = json.Unmarshal(content, &yahoo)
	if err != nil {
		return nil, err
	}

	record := yahoo.QuoteResponse.Result[0]

	response := &src.BrentOilStock{
		Price:    record.Bid,
		Currency: record.Currency,
		Date:     time.Unix(record.RegularMarketTime, 0),
		Timezone: record.ExchangeTimezoneName,
	}

	return response, nil
}
