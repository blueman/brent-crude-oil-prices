package today

import "time"

type YahooFinanceQuote struct {
	QuoteResponse QuoteResponse `json:"quoteResponse"`
}

type QuoteResponse struct {
	Result []QuoteResult `json:"result"`
	Error  interface{}   `json:"error"`
}

type QuoteResult struct {
	Language                          string    `json:"language"`
	Region                            string    `json:"region"`
	QuoteType                         string    `json:"quoteType"`
	TypeDisp                          string    `json:"typeDisp"`
	QuoteSourceName                   string    `json:"quoteSourceName"`
	Triggerable                       bool      `json:"triggerable"`
	CustomPriceAlertConfidence        string    `json:"customPriceAlertConfidence"`
	HeadSymbolAsString                string    `json:"headSymbolAsString"`
	ContractSymbol                    bool      `json:"contractSymbol"`
	Currency                          string    `json:"currency"`
	ExchangeDataDelayedBy             int       `json:"exchangeDataDelayedBy"`
	ExchangeTimezoneName              string    `json:"exchangeTimezoneName"`
	ExchangeTimezoneShortName         string    `json:"exchangeTimezoneShortName"`
	GmtOffSetMilliseconds             int       `json:"gmtOffSetMilliseconds"`
	EsgPopulated                      bool      `json:"esgPopulated"`
	Tradeable                         bool      `json:"tradeable"`
	CryptoTradeable                   bool      `json:"cryptoTradeable"`
	FirstTradeDateMilliseconds        int64     `json:"firstTradeDateMilliseconds"`
	PriceHint                         int       `json:"priceHint"`
	RegularMarketChange               float64   `json:"regularMarketChange"`
	RegularMarketChangePercent        float64   `json:"regularMarketChangePercent"`
	RegularMarketTime                 int64     `json:"regularMarketTime"`
	RegularMarketPrice                float64   `json:"regularMarketPrice"`
	RegularMarketDayHigh              float64   `json:"regularMarketDayHigh"`
	RegularMarketDayRange             string    `json:"regularMarketDayRange"`
	RegularMarketDayLow               float64   `json:"regularMarketDayLow"`
	RegularMarketVolume               int       `json:"regularMarketVolume"`
	RegularMarketPreviousClose        float64   `json:"regularMarketPreviousClose"`
	Bid                               float64   `json:"bid"`
	Ask                               float64   `json:"ask"`
	BidSize                           int       `json:"bidSize"`
	AskSize                           int       `json:"askSize"`
	Exchange                          string    `json:"exchange"`
	Market                            string    `json:"market"`
	FullExchangeName                  string    `json:"fullExchangeName"`
	ShortName                         string    `json:"shortName"`
	RegularMarketOpen                 float64   `json:"regularMarketOpen"`
	AverageDailyVolume3Month          int       `json:"averageDailyVolume3Month"`
	AverageDailyVolume10Day           int       `json:"averageDailyVolume10Day"`
	FiftyTwoWeekLowChange             float64   `json:"fiftyTwoWeekLowChange"`
	FiftyTwoWeekLowChangePercent      float64   `json:"fiftyTwoWeekLowChangePercent"`
	FiftyTwoWeekRange                 string    `json:"fiftyTwoWeekRange"`
	FiftyTwoWeekHighChange            float64   `json:"fiftyTwoWeekHighChange"`
	FiftyTwoWeekHighChangePercent     float64   `json:"fiftyTwoWeekHighChangePercent"`
	FiftyTwoWeekLow                   float64   `json:"fiftyTwoWeekLow"`
	FiftyTwoWeekHigh                  float64   `json:"fiftyTwoWeekHigh"`
	MarketState                       string    `json:"marketState"`
	UnderlyingSymbol                  string    `json:"underlyingSymbol"`
	UnderlyingExchangeSymbol          string    `json:"underlyingExchangeSymbol"`
	OpenInterest                      int       `json:"openInterest"`
	ExpireDate                        int       `json:"expireDate"`
	ExpireIsoDate                     time.Time `json:"expireIsoDate"`
	FiftyDayAverage                   float64   `json:"fiftyDayAverage"`
	FiftyDayAverageChange             float64   `json:"fiftyDayAverageChange"`
	FiftyDayAverageChangePercent      float64   `json:"fiftyDayAverageChangePercent"`
	TwoHundredDayAverage              float64   `json:"twoHundredDayAverage"`
	TwoHundredDayAverageChange        float64   `json:"twoHundredDayAverageChange"`
	TwoHundredDayAverageChangePercent float64   `json:"twoHundredDayAverageChangePercent"`
	SourceInterval                    int       `json:"sourceInterval"`
	Symbol                            string    `json:"symbol"`
}
