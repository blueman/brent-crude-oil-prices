package src

import "time"

type BrentOilStock struct {
	Price    float64
	Currency string
	Date     time.Time
	Timezone string
}
