package src

import (
	"encoding/json"
	"strings"
	"time"
)

const (
	TodayFile   = "data/brent-today.json"
	DailyFile   = "data/brent-daily.json"
	MonthlyFile = "data/brent-monthly.json"

	brentPriceDateFormat = "2006-01-02"
)

type BrentPrice struct {
	Date  CustomDateFormat `json:"date"`
	Price float64          `json:"price"`
}

type CustomDateFormat time.Time

func (j CustomDateFormat) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(j).Format(brentPriceDateFormat))
}

func (j CustomDateFormat) Time() time.Time {
	return time.Time(j)
}

func (j *CustomDateFormat) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")
	t, err := time.Parse(brentPriceDateFormat, s)
	if err != nil {
		return err
	}
	*j = CustomDateFormat(t)
	return nil
}

func (j CustomDateFormat) Format(s string) string {
	t := time.Time(j)
	return t.Format(s)
}
