package src

import "math"

func CheckError(e error) {
	if e != nil {
		panic(e)
	}
}

func RoundToNearestTwoDecimals(num float64) float64 {
	return math.Round(num*100) / 100
}
